﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathClasses
{
    public class Matrix3
    {
        public float m1, m2, m3, m4, m5, m6, m7, m8, m9;

        public Matrix3()
        {
            m1 = 1; m2 = 0; m3 = 0;
            m4 = 0; m5 = 1; m6 = 0;
            m7 = 0; m8 = 0; m9 = 1;
        }

        public Matrix3(float a1, float a2, float a3, float a4, float a5, float a6, float a7, float a8, float a9)
        {
            m1 = a1; m2 = a2; m3 = a3;
            m4 = a4; m5 = a5; m6 = a6;
            m7 = a7; m8 = a8; m9 = a9;
        }

        public void SetRotateX(double radians)
        {
            this.m1 = 1; this.m2 = 0; this.m3 = 0;
            this.m4 = 0; this.m5 = (float)Math.Cos(radians); this.m6 = (float)Math.Sin(radians);
            this.m7 = 0; this.m8 = (float)-Math.Sin(radians); this.m9 = (float)Math.Cos(radians);
        }
        public void SetRotateY(double radians)
        {
            this.m1 = (float)Math.Cos(radians); this.m2 = 0; this.m3 = (float)-Math.Sin(radians);
            this.m4 = 0; this.m5 = 1; this.m6 = 0;
            this.m7 = (float)Math.Sin(radians); this.m8 = 0; this.m9 = (float)Math.Cos(radians);
        }
        public void SetRotateZ(double radians)
        {
            this.m1 = (float)Math.Cos(radians); this.m2 = (float)Math.Sin(radians); this.m3 = 0;
            this.m4 = (float)-Math.Sin(radians); this.m5 = (float)Math.Cos(radians); this.m6 = 0;
            this.m7 = 0; this.m8 = 0; this.m9 = 1;
        }
    }
}